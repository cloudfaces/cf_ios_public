$(document).on('cf-initialized', function() {
    setInterval(function() {
        $('.ain').text('AIN: '+CloudFaces.DeviceCode.getDeviceAin());
        $('.token').text('Token: '+CloudFaces.DeviceCode.getDeviceToken());
        $('.devCode').text('Device Code: '+CloudFaces.DeviceCode.getDeviceCode());
        $('.reloaded').text('Last reload: '+ new Date().toGMTString());
    }, 5000);
});

$(document).on('onFenceEntered', function(fence) {
    alert('Fence entered: '+JSON.stringify(fence));
});

$(document).on('onFenceExited', function(fence) {
    alert('Fence exited: '+JSON.stringify(fence));
});

$('.show-fences').on('click', function() {
    CFLocation.getFences(function(fences) {
        $('.fences').html('');
        if (fences != undefined && Array.isArray(fences) && fences.length > 0) {
            fences.forEach(function (fen) {
                var fenceHtml = 
                    '<p>ID: '+fen.id+'</p>'+
                    '<p>Radius: '+fen.radius+'</p>'+
                    '<p>Lat: '+fen.lat+'</p>'+
                    '<p>Long: '+fen.long+'</p>'+
                    '<p>=====</p>';
            
                $('.fences').append(fenceHtml);
            });
        } else {
            $('.fences').append('No fences returned from NB.');
        }
    });
});

// $('.log123').on('click', function() {
//     CloudFaces.Helpers.log(123);
// })