var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

(function() {
    document.addEventListener("DOMContentLoaded", function(event) {
        var src, cssClass;

        var scr = document.createElement('script');
        console.log("DOM fully loaded and parsed");
        var dirPrefix = ''; //../
        if (location.pathname.indexOf('customModules') >= 0) {
            dirPrefix = '../modules/';
        }

        var nativeLocation = 'native/'; //appCore/nativeBridge/

//        if (typeof CloudFaces.Config.web != 'undefined' && CloudFaces.Config.web === 1){
//            src = dirPrefix + nativeLocation + 'native_bridge_web.js';
//            cssClass = 'web';
//        } else {
            if (isMobile.Android()) {
                src = dirPrefix + nativeLocation + 'native_bridge_android.js';
                cssClass = 'android';
            } else if (isMobile.iOS()) {
                src = dirPrefix + nativeLocation + 'native_bridge_ios.js';
                cssClass = 'ios';
            } else {
                console.error('It is not web, but it is unknown platform');
            }
//        }

        scr.setAttribute('src', src);
        document.body.className = (document.body.className + ' ' + cssClass).trim();
        document.head.appendChild(scr);
    });
})();
