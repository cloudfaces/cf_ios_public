var CFVars = {
    navigationContext: '',
    variable: {},
    triggered: false
};

window.addEventListener("message", function(e) {
    if (typeof e.data.indexOf !== 'undefined') {
        if (e.data.indexOf('context:') === 0) {
            CFVars.navigationContext = e.data.replace('context:', '');

            if (!CFVars.triggered) {
                $(document).trigger('cf-ready');
                CFVars.triggered = true;
            }
        } else if (e.data.indexOf('variable:') === 0) {
            CFVars.variable = JSON.parse(e.data.replace('variable:', ''));
        }
    } else if (typeof e.data === 'object') {
        var d = e.data.args;
        var skipSendMessage = e.data.source !== '' ? true : false;
        if (e.data.method === 'variableWrite') {
            CFVariableStorage.writeValue(d.key, d.value, skipSendMessage);
        }
    }
});

function sendMessage(method, args, source) {
    return window.parent.postMessage({
        method: method,
        args: args || {},
        source: source || ''
    }, 'http://app.cloudfaces.io/');
}

var CFNavigation = {
    navigate: function navigate(page, context) {
        sendMessage('navigation:navigate', {
            page: page,
            context: context || ''
        });
    },

    getNavigationContext: function getNavigationContext() {
        return CFVars.navigationContext;
    },

    navigateBack: function navigateBack(result) {
        sendMessage('navigation:back');
    }
};

var CFMenuNavigation = {
    navigate: function navigate(page, context) {
        return CFNavigation.navigate(page, context);
    },

    getNavigationContext: function getNavigationContext() {
        return CFNavigation.getNavigationContext();
    },

    navigateBack: function navigateBack(result) {
        return CFNavigation.navigateBack(result);
    }
};

function CFHttpRequest() {
	this.Method = "GET";
	this.Body = "";
	this.Url = "http://localhost";
	this.Headers = {};
	
	this.setRequestHeader = function(header, value) {
		this.Headers[header] = value;
	};
	
	this.send = function(callback) {
		callback.call(this, null);
	}
};

var CFVariableStorage = {

  valueExists : function valueExists(key) {    
    return false;
  },

  writeValue : function writeValue(key, value) {

  },
  
  readValue : function readValue(key) {
  	return null;
  }
};

var CFPersistentStorage = {

  valueExists : function valueExists(key) {    
    return false;
  },

  writeValue : function writeValue(key, value) {

  },
  
  readValue : function readValue(key) {
  	return null;
  }
};


var CFScanner = {
	scanBarCode : function scanBarCode(callback) {
		callback.call(null, "dummy 123456");
	},
};

var CFFacebook = {
	// callback: function(token)
	login : function login(callback) {
		callback.call(null, "dummy login token");
	},
	
	isLoggedIn : function isLoggedIn() {
		return true;
	},
	
	getToken : function getToken() {
		return "dummy token";
	},
};

var CFLocation = {
    // callback is
    getCurrentPosition: function(callback) {
        return null;
    },
    
    startTracking : function(url) {
       return null;
    },
    
    stopTracking : function() {
        return null;
    },
    
    getLocationQueue : function(callback) {
    	return null;
    },
    
    getFences : function(callback) {
    	return null;
    },
    
    addFence : function(fence) {
    	return null;
    },
    
    removeFence : function(fence) {
    	return null;
    }
};

$(document).trigger( 'cf-loaded' );