
function setStrings(reset, callback) {
    CFRuntime.log("callback: " + callback);
    CFRuntime.findPage("menu", function(page) {
        if (page) {
            page.title = "Hauptmenü";
            
            CFRuntime.updatePage("menu", reset ? null : page);
        }
    });
    CFRuntime.findPage("contactpage", function(page) {
        if (page) {
            page.title = "Über die App";
            page.pageHeader.rightButton.text = "Kontakt";
            
            CFRuntime.updatePage("contactpage", reset ? null : page);
        }
    });
    CFRuntime.findPage("geopage", function(page) {
        if (page) {
            page.title = "Geo Seite";
            
            CFRuntime.updatePage("geopage", reset ? null : page);
        }
    });
    CFRuntime.findPage("geopage2", function(page) {
        if (page) {
            page.title = "Verfolgung";
            
            CFRuntime.updatePage("geopage2", reset ? null : page);
        }
    });
    CFRuntime.findPage("geopage3", function(page) {
        if (page) {
            page.title = "Gebiete";
            
            CFRuntime.updatePage("geopage3", reset ? null : page);
        }
    });
    CFRuntime.findPage("notifications", function(page) {
        if (page) {
            page.title = "Benachrichtigungen";
            
            CFRuntime.updatePage("notifications", reset ? null : page);
        }
    });
    CFRuntime.findPage("qrcode", function(page) {
        if (page) {
            page.title = "QR Code Leser";
            page.pageHeader.rightButton.text = "Lesen";
            
            CFRuntime.updatePage("qrcode", reset ? null : page);
        }
    });
    CFRuntime.findPage("navigation_master", function(page) {
        if (page) {
            page.title = "Hauptansicht";
            page.pageHeader.rightButton.text = "Neu";
            
            CFRuntime.updatePage("navigation_master", reset ? null : page);
        }
    });
    CFRuntime.findPage("navigation_detail", function(page) {
        if (page) {
            page.title = "Details";
            page.pageHeader.rightButton.text = "Entfernen";
            
            CFRuntime.updatePage("navigation_detail", reset ? null : page);
        }
    });
    CFRuntime.findPage("take_picture", function(page) {
        if (page) {
            page.title = "Aufnahme";
            
            CFRuntime.updatePage("take_picture", reset ? null : page);
        }
    });
    CFRuntime.findPage("view_picture", function(page) {
        if (page) {
            page.title = "Bild ansehen";
            page.pageHeader.rightButton.text = "Löschen";
            
            CFRuntime.updatePage("view_picture", reset ? null : page);
        }
    });
    CFRuntime.findPage("device_identifier", function(page) {
        if (page) {
            page.title = "Geräte ID";
            
            CFRuntime.updatePage("device_identifier", reset ? null : page);
        }
    });
    CFRuntime.findPage("share", function(page) {
        if (page) {
            page.title = "Teilen";
            
            CFRuntime.updatePage("share", reset ? null : page);
        }
    });
    CFRuntime.findPage("external", function(page) {
        if (page) {
            page.title = "Externe Inhalte";
            
            CFRuntime.updatePage("external", reset ? null : page);
        }
    });
    CFRuntime.findPage("runtime", function(page) {
        if (page) {
            page.title = "Laufzeit";
            
            CFRuntime.updatePage("runtime", reset ? null : page);
        }
    });
    CFRuntime.findPage("styleroot", function(page) {
        if (page) {
            page.title = "Stile";
            
            CFRuntime.updatePage("styleroot", reset ? null : page);
        }
    });

    CFRuntime.findPage("tabs", function(page) {
        if (page) {
            
            page.tabItems[0].title = "Hauptmenü";
            page.tabItems[1].title = "Stile";
            page.tabItems[2].title = "Liste";
            page.tabItems[3].title = "Über";
            
            CFRuntime.updatePage("tabs", reset ? null : page);
        }
        
        if (callback != null) {
            callback();
        }
    });
}

/***************
 * styles
 ***************/



function setStyle(style, callback) {
    CFRuntime.findPage("menu", function(page) {
        if (page) {
            CFRuntime.updatePage("menu", page);
        }
    });
    CFRuntime.findPage("menupage", function(page) {
        if (page) {
            page.pageHeader = new CFPageHeader();
            page.pageHeader.backgroundColor = "#FF0000";
            if (style == 2) {
                page.pageHeader.rightButton = new CFHeaderButton();
                page.pageHeader.rightButton.icon = "trash.png";
                page.pageHeader.rightButton.textColor = "#FFFF0000";
                page.pageHeader.rightButton.function = "onDeletePressed";
                page.pageHeader.rightButton.text = "Delete";
            }
            CFRuntime.updatePage("menupage", page);
        }
    });
    CFRuntime.findPage("contactpage", function(page) {
        if (page) {
            page.pageHeader.backgroundColor = null;
            page.pageHeader.rightButton.icon = "mail.png";
            CFRuntime.updatePage("contactpage", page);
        }
    });
    CFRuntime.findPage("geopage", function(page) {
        if (page) {
            CFRuntime.updatePage("geopage", page);
        }
    });
    CFRuntime.findPage("geopage2", function(page) {
        if (page) {
            CFRuntime.updatePage("geopage2", page);
        }
    });
    CFRuntime.findPage("geopage3", function(page) {
        if (page) {
            CFRuntime.updatePage("geopage3", page);
        }
    });
    CFRuntime.findPage("notifications", function(page) {
        if (page) {
            CFRuntime.updatePage("notifications", page);
        }
    });
    CFRuntime.findPage("qrcode", function(page) {
        if (page) {
            CFRuntime.updatePage("qrcode", page);
        }
    });
    CFRuntime.findPage("navigation_master", function(page) {
        if (page) {
            CFRuntime.updatePage("navigation_master", page);
        }
    });
    CFRuntime.findPage("navigation_detail", function(page) {
        if (page) {
            CFRuntime.updatePage("navigation_detail", page);
        }
    });
    CFRuntime.findPage("take_picture", function(page) {
        if (page) {
            CFRuntime.updatePage("take_picture", page);
        }
    });
    CFRuntime.findPage("view_picture", function(page) {
        if (page) {
            CFRuntime.updatePage("view_picture", page);
        }
    });
    CFRuntime.findPage("device_identifier", function(page) {
        if (page) {
            CFRuntime.updatePage("device_identifier", page);
        }
    });
    CFRuntime.findPage("share", function(page) {
        if (page) {
            CFRuntime.updatePage("share", page);
        }
    });
    CFRuntime.findPage("external", function(page) {
        if (page) {
            CFRuntime.updatePage("external", page);
        }
    });
    CFRuntime.findPage("runtime", function(page) {
        if (page) {
            CFRuntime.updatePage("runtime", page);
        }
    });
    CFRuntime.findPage("styleroot", function(page) {
        if (page) {
            if (style == 1) {
                page.pageHeader.rightButton = new CFHeaderButton();
                page.pageHeader.rightButton.icon = "trash.png";
                page.pageHeader.rightButton.textColor = "#FFFF0000";
                page.pageHeader.rightButton.function = "onDeletePressed";
                page.pageHeader.rightButton.text = "Delete";
                page.pageHeader.titleImage = "logo.png";
                page.pageHeader.statusBarColor = "#FF000000";
            }
            page.pageHeader.backgroundImage = null;
            page.pageHeader.backgroundColor = "#00FF00";
            page.pageHeader.titleColor = "#FF0000";
            
            CFRuntime.updatePage("styleroot", page);
        }
    });
    CFRuntime.findPage("tabs", function(page) {
        if (page) {
            page.pageHeader = new CFPageHeader();
            page.pageHeader.backgroundColor = "#FF0000";
            if (style == 2) {
                page.pageHeader.rightButton = new CFHeaderButton();
                page.pageHeader.rightButton.icon = "trash.png";
                page.pageHeader.rightButton.textColor = "#FFFF0000";
                page.pageHeader.rightButton.function = "onDeletePressed";
                page.pageHeader.rightButton.text = "Delete";
            }
            
            page.tabItems[0].icon = "global_l.png";
            page.tabItems[1].icon = "look_l.png";
            page.tabItems[2].icon = "drawer_l.png";
            page.tabItems[3].icon = "info_l.png";
            
            CFRuntime.updatePage("tabs", page);
        }
        if (callback != null) {
            callback();
        }
    });
}

function germanStrings(callback) {
    setStrings(false, callback);
}

function resetStrings(callback) {
    setStrings(true, callback);
}

function initPageLanguage() {
    var language = CFPersistentStorage.readValue("language");
    var style = CFPersistentStorage.readValue("style");
    
    var applyStyleFunction = function() {
        if (style == "style1") {
            CFRuntime.log("set style to 1");
            setStyle(1, null);
        } else if (style == "style2") {
            CFRuntime.log("set style to 2");
            setStyle(2, null);
        }
    };
    
    CFRuntime.log("reset pages");
    resetStrings(function() {
        if (language == "de") {
            CFRuntime.log("switch to de");
            
            germanStrings(applyStyleFunction);
        } else {
            applyStyleFunction();
        }
    });
    
    
}

$(document).on('cf-loaded', function() {
    CFNotification.addObserverForName('language_changed', function() {
        initPageLanguage();
    });
    CFNotification.addObserverForName('style_changed', function() {
        initPageLanguage();
    });
    initPageLanguage();
});

