//
//  AppDelegate.h
//  mobile-pocket
//
//  Created by Simon Moser on 30.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMenu.h"
#import "LocationTracker.h"
#import "Navigation.h"
#import "BadgeBarButtonItem.h"
//#import <Bolts.h>
#import <UserNotifications/UserNotifications.h>
#import <WebKit/WebKit.h>
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

{
    UIViewController* mainMenu;
    NSString *getAppBadgeNum;
}
@property (nonatomic, strong) UINavigationController *navController;
@property (assign, nonatomic) BOOL shouldRotate;


@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (strong, nonatomic) UIViewController *mainMenu;
@property NSInteger badgeNum;
- (NSString *)getAppBadgeNum;
@property (strong, nonatomic) NSString *getAppBadgeNum;
@property (strong, nonatomic) NSString *isUseBackground;
@property (strong, nonatomic) NSString *isUseLocation;
@property (strong, nonatomic) NSString *isUseBackgroundLocation;

#ifdef FEATURE_CHOOSESERVERADDRESS
@property  BOOL showServerAddressChooser;
#endif

//Location
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (strong,nonatomic) LocationShareModel * shareModel;
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window;
-(void) codePush;
@end
