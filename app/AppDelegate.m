//
//  AppDelegate.m
//  mobile-pocket
//
//  Created by Simon Moser on 30.10.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
@import UIKit;

#import "AppDelegate.h"
#import "AppDelegate+Configuration.h"
#import "MainMenu.h"
#import "SplashViewController.h"
#import "CFPage.h"
#import "LocationFencer.h"
#import "Badges.h"
#import "SlidingViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import "PayPalMobile.h"
#import <Google/Analytics.h>
#import "ACTReporter.h"
#import "BackgroundLocationRetriever.h"
//#import <FirebaseCore/FirebaseCore.h>
#import "AppDownloadViewController.h"
#import "AppDownloadViewHelper.h"
//#import <BeaconService/BIBeaconService.h>
#import "WebviewViewController.h"
#define kPushNotificationRequestAlreadySeen @"PushNotificationRequestAlreadySeen"
#import "MyURLProtocol.h"
#import "CFDynamicFileUpdate.h"
#import "CFHttpRequest.h"
@import UserNotifications;
#import "Storage.h"
#import "DataPool.h"
#import "MRProgress.h"
#import "CFHttpResponseHelper.h"
#import "CFRuntime.h"
#import "SlidingTabsHelper.h"
@interface AppDelegate (Private)
- (void) updatePushNotifcation;
- (void) initLogger;
- (void) showDialog:(NSInteger) dialogId;
@end

@implementation AppDelegate

LocationShareModel*getSharedInstance;
BackgroundLocationRetriever *backgroundLocation;

@synthesize window = _window;
@synthesize mainMenu;
@synthesize badgeNum;
@synthesize getAppBadgeNum;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [LocationFencer instance];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
#ifdef FRANK_CUCUMBER
    // speed up animations for ui testing to make tests faster
    self.window.layer.speed = 5;
#endif
    [self initAppearance];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    self.mainMenu = [[MainMenu alloc] init];
    self.window.rootViewController = self.mainMenu;
    //[self.window addSubview:mainMenu.view];
    
    [self.window makeKeyAndVisible];
    
    
#ifdef FEATURE_CHOOSESERVERADDRESS
    if (self.showServerAddressChooser) {
        [self chooseServerAddress];
    }
#endif
    
    if ([BundleSettings pushNotificationsAreActive]) {
        
        [self registerForRemoteNotifications];
        
        
        //handles background receiv of push message before ios8
        NSDictionary *notification= [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
        if (notification) {
            [self handlePushNotifcation:notification identifier:nil];
        }
        
        
        // update the app setting for push notifcation
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePushNotifcation) name:PUSH_NOTIFCATION_CHANGED object:nil];
        
        
    }
    
    //plist
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    
    _isUseBackground         =  infoPlistDict[@"IsBackgroundApp"];
    _isUseLocation           =  infoPlistDict[@"IsUseLocation"];
    _isUseBackgroundLocation =  infoPlistDict[@"IsUseBackgroundLocation"];
    
    if ([_isUseBackground isEqual:@"YES"]) {
        NSLog(@"Background fetch is enable");
        
    }
    
    if ([_isUseLocation isEqual:@"YES"]) {
        
        NSLog(@"App use location is enable");
        
        self.locationTracker = [[LocationTracker alloc]init];
        [self.locationTracker startLocationTracking];
    }
    
    
    if ([_isUseBackgroundLocation isEqual:@"YES"]) {
        
        NSLog(@"Background location is enable");
    }
    
    //FB
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //GoogleAnalytics
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    if (!configureError) {
        
        NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
        
        // Optional: configure GAI options.
        GAI *gai = [GAI sharedInstance];
        gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
        gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
        
    }
    
    
    //Google Adwords
    NSDictionary *conversionTracking = infoPlistDict[@"Conversion Tracking"];
    
    NSNumber *conversionTrackingBool = (NSNumber *)[conversionTracking objectForKey:@"isEnable"];
    if([conversionTrackingBool boolValue] == YES) {
        
        NSString *isRepeatable = [conversionTracking objectForKey:@"isRepeatable"];
        NSString *conversionId = [conversionTracking objectForKey:@"conversionId"];
        NSString *conversionLabel = [conversionTracking objectForKey:@"conversionLabel"];
        NSString *conversionValue = [conversionTracking objectForKey:@"conversionValue"];
        
        // Enable automated usage reporting.
        [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:conversionId];
        
        if ([isRepeatable isEqualToString:@"NO"]) {
            [ACTConversionReporter reportWithConversionID:conversionId label:conversionLabel value:conversionValue isRepeatable:NO];
            
        }else if ([isRepeatable isEqualToString:@"YES"]) {
            [ACTConversionReporter reportWithConversionID:conversionId label:conversionLabel value:conversionValue isRepeatable:YES];
        }
    }
    
    
    
#ifdef FIREBASE
    NSString *IsFirebaseEnable = infoPlistDict[@"IsFirebaseEnable"];
    
    if ([IsFirebaseEnable isEqual:@"YES"]) {
        
        [FIRApp configure];
    }
#endif
    
    //#if TARGET_IPHONE_SIMULATOR == 0
#ifdef LOG2FILE1
    // All logs to file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName =[NSString stringWithFormat:@"appLog.log"];
    NSString *logFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
#endif
    
#ifdef BEACONS
    //in preprocessor
    
    [BIBeaconService requestAuthorization];
    [BIBeaconService initWithToken:@"q5mnSeQ75Aq4x9kKqh3R"];
    
#endif
    
    
#ifdef CODEPUSH
    [self codePush];
#endif
    
    return YES;
}

- (void) codePush {
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *nativeVersion = infoPlistDict[@"CFNativeVersion"];
    
    NSString *appHashUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"app_hash_url"];
    appHashUrl = [appHashUrl stringByAppendingString: @"native/"];
    appHashUrl = [appHashUrl stringByAppendingString: nativeVersion];
    appHashUrl = [appHashUrl stringByAppendingString: @"/"];

    if (appHashUrl==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"codePushFileCompare"];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isManifest"];
        [[NSUserDefaults standardUserDefaults] setObject:@"-1" forKey:@"app_version"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
//        NSString *urlSrt = appHashUrl;
//        NSDictionary *response = [self getDataFrom:urlSrt];
//        NSString *status = response[@"Status"];
//
//        if ([status isEqual:@"Success"]) {
//            NSDictionary *responseData = response[@"Data"];
//            NSString *osbu = responseData[@"object_storage_base_url"];
//            NSString *newVersion = responseData[@"version"];
//
//            // set object storage base url - https://cdn.cloudfaces.com
//            [[NSUserDefaults standardUserDefaults] setObject: osbu forKey:@"object_storage_base_url"];
//
//            // Check version with current version
//            NSString *appVerison = [[NSUserDefaults standardUserDefaults]
//                                    stringForKey:@"app_version"];
//
//            if (![newVersion isEqual:appVerison] ) {
//                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"isManifest"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//
//                [CFDynamicFileUpdate sharedInstance].appVersionApiCall = newVersion;
//
//                NSString *manifestSHA1 = responseData[@"object_key"];
//                [[CFDynamicFileUpdate sharedInstance] downoadManifest:manifestSHA1];
//            } else {
//                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isManifest"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//            }
//
//        } else {
//            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isManifest"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//
//        }
//
    }
    
}

- (NSDictionary *) getDataFrom:(NSString *)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %li", url, (long)[responseCode statusCode]);
        return nil;
    }
    return [NSJSONSerialization JSONObjectWithData:oResponseData options:kNilOptions error:&error];
}

//BOOL shouldRotate;
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    
    NSString *orientation = [CFRuntime instance].orientation;
    
    NSLog(@"orientation %@", orientation);
    
    if ([orientation isEqual:@"landscape"]){
        return UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskLandscapeLeft;
    }
    if ([orientation isEqual:@"all"]){
        return UIInterfaceOrientationMaskAll;
    }
    else {
        return UIInterfaceOrientationMaskPortrait;
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // [[[BundleSettings instance] managedObjectContext] saveToPersistentStoreAndWait];
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    if ([_isUseBackground isEqual:@"YES"]) {
        [self.locationTracker applicationEnterBackground];
    }
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    
    if ([_isUseBackground isEqual:@"YES"]) {
        [self.locationTracker applicationEnterBackground];
    }
    if ([_isUseBackgroundLocation isEqual:@"YES"]) {
        [self.locationTracker startMonitoringLocation];
        
    }
    
    [self saveLog];
    
}
- (void) saveLog {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].logArray
                     forKey:@"logArray"];
    [userDefaults synchronize];
    
}

// During the Facebook login flow, your app passes control to the Facebook iOS app or Facebook in a mobile browser.
// After authentication, your app will be called back with the session information.
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

#pragma Push Notification Handling

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSUInteger length = deviceToken.length;
    const unsigned char *buffer = deviceToken.bytes;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i = 0; i < length; ++i) {
        [hexString appendFormat:@"%02x", buffer[i]];
    }
    NSString *pushtoken = [hexString copy];
    [BundleSettings setPushToken:pushtoken];
    
    //TODO UPDATE ON SERVER
    //[[RKObjectManager sharedManager] putObject:[self createClient:pushtoken] delegate:self];
}

- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"" message:[error localizedDescription] cancelButtonItem:[RIButtonItem itemWithLabel:@"OK"] otherButtonItems: nil] show];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [self handlePushNotifcation:userInfo identifier:nil];
    
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber -1 ;
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"Background");
        
        //Refresh the local model
        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber +1 ;
        
    } else {
        
        NSLog(@"Active");
        //  [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber -1 ;
        //Show an in-app banner
        
    }
    
}

/**
 Required to handle local notifications triggered by beacons
 */
- (void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    if (application.applicationState == UIApplicationStateActive) {
        
        NSLog(@"Alert Message:  ACTIVE %@", notification.alertTitle);
    }
    else if(application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive)
    {
        [[NSUserDefaults standardUserDefaults] setObject:notification.alertTitle forKey:@"alertmsg"];
    }
    
    [self handlePushNotifcation:notification.userInfo identifier:nil];
    
//    [BIBeaconService handleNotification:notification];
}

//ios8 on this will be called
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    [self handlePushNotifcation:userInfo identifier:identifier];
    if (completionHandler) {
        completionHandler();
    }
}

- (void) applicationDidBecomeActive:(UIApplication *)application{
    //[[FacebookHelper instance] handleDidBecomeActive];
    [FBSDKAppEvents activateApp];
    
}

-(void) handlePushNotifcation:(NSDictionary *)userInfo  identifier:(NSString *)identifier
{
    
    NSDictionary* action = [userInfo valueForKey:@"cfpushaction"];
    if (!action) {
        //  [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_MESSAGE_OPENED object:nil userInfo:userInfo];
        
        return; // there is no action to handle
    }
    NSString* actionType = [action valueForKey:@"type"];
    NSDictionary* params = [action valueForKey:@"parameters"];
    if ([actionType isEqualToString:@"CFNavigation.navigate"] && params != nil) {
        NSString* pageId = [params valueForKey:@"pageId"];
        if (pageId != nil) {
            CFPage* page = [[CFRuntime instance] getPageWithId:pageId];
            
            [CFRuntime presentPage:page];
        }
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_MESSAGE_OPENED object:nil userInfo:action];
    }
}
+ (void)handleNotification:(UILocalNotification*)notification
{
    NSLog(@"handleNotification %@", notification);
}
/**
 Allow background updates to load up-to-date beacon and campaign data
 */
//- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    [BIBeaconService performFetchWithCompletionHandler:completionHandler];
//}

- (void) application: (UIApplication *) application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"UIBackgroundFetchResult))completionHandler");
}

- (void)registerForRemoteNotifications {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else {
        // Code for old versions
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge  ) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
        
        if(![[NSUserDefaults standardUserDefaults] boolForKey:kPushNotificationRequestAlreadySeen]) {
            
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kPushNotificationRequestAlreadySeen];
        }
        else {
            // Already allowed -> register without notifying the user
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
        }
    }
}

//Called when a notification is delivered to a foreground app.

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info willPresentNotification: %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info didReceiveNotificationResponse : %@",response.notification.request.content.userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_MESSAGE_OPENED object:nil userInfo:response.notification.request.content.userInfo];
    completionHandler();
}


-(BOOL) shouldAutorotate {
    
    // Return YES for supported orientations
    return YES;
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (UIInterfaceOrientationPortrait |  UIInterfaceOrientationLandscapeLeft);
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}
@end

