//
//  BundleSettings+Additions.m
//  app
//
//  Created by Simon Moser on 04.04.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BundleSettings+Additions.h"

@implementation BundleSettings (Additions)

+(BundleSettings *) instance {
    static dispatch_once_t pred;
    static BundleSettings *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[BundleSettings alloc] init];
    });
    return shared;
}


+(NSString*) getTargetConfigValueWithName:(NSString*) keyName
{
    static dispatch_once_t pred;
    static NSDictionary *targetConfigDictionary = nil;
    dispatch_once(&pred, ^{
        NSString* targetConfiglistPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@-config" , @"app"] ofType:@"plist"];
        targetConfigDictionary = [NSDictionary dictionaryWithContentsOfFile:targetConfiglistPath];
    });
	NSString* result = [targetConfigDictionary valueForKey:keyName];
    return result;
}

+ (void) setOverrideRemoteApp:(BOOL) overrideSetting
{
    BundleSettings* bs = [BundleSettings instance];
    bs->_overrideRemoteApp = overrideSetting;
}

+ (BOOL) isRemoteApp
{
    BundleSettings* bs = [BundleSettings instance];
    return [[BundleSettings getTargetConfigValueWithName:@"RemoteApp"] boolValue] || bs->_overrideRemoteApp;
}

+ (BOOL) pushNotificationsAreActive
{
    return [[BundleSettings getTargetConfigValueWithName:@"PushNotificationsAreActive"] boolValue];
}

+ (void) setPushToken:(NSString*) newToken
{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:newToken forKey:@"PushToken"];
    [preferences synchronize];
}

+ (NSString*) pushToken
{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences objectForKey:@"PushToken"];
}

+ (NSString*) fencingUrl
{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences objectForKey:@"FencingUrl"];
}

+ (void) setFencingUrl:(NSString*) url
{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:url forKey:@"FencingUrl"];
    [preferences synchronize];
}

+ (NSString*) lastVersion
{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences objectForKey:@"LastVersion"];
}

+ (void) setLastVersion:(NSString *)version
{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:version forKey:@"LastVersion"];
    [preferences synchronize];
}

@end
