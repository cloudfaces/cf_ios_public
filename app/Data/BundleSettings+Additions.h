//
//  BundleSettings+Additions.h
//  app
//
//  Created by Simon Moser on 04.04.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BundleSettings.h"

@interface BundleSettings (Additions)


+(BundleSettings*) instance;
+(NSString*) getTargetConfigValueWithName:(NSString*) keyName;

+ (void) setOverrideRemoteApp:(BOOL) overrideSetting;
+ (BOOL) isRemoteApp;
+ (BOOL) pushNotificationsAreActive;

+ (NSString*) pushToken;
+ (void) setPushToken:(NSString*) newToken;

+ (NSString*) fencingUrl;
+ (void) setFencingUrl:(NSString*) url;

+ (NSString*) lastVersion;
+ (void) setLastVersion:(NSString*) version;

@end
