//
//  CFSliderPage.h
//  app
//
//  Created by Bernhard Kunnert on 05.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "TypedPage.h"
#import "BSJsonSupported.h"

@interface CFSliderPage : NSObject <TypedPage, BSJsonSupported>

@property (nonatomic, readonly) CFPage* menuPage;
@property (nonatomic, readonly) CFPage* contentPage;

- (id) initWithPage:(CFPage *)page;

@property (strong, nonatomic) NSString* slide;
@property (strong, nonatomic) NSString* tap;
@property (strong, nonatomic) NSString* direction;

@end
