//
//  PayPalViewController.m
//  app
//
//  Created by LeyLa on 3/24/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

//#import "PayPalViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "WebviewViewController.h"

// Set the environment:
// - For live charges, use PayPalEnvironmentProduction (default).
// - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
// - For testing, use PayPalEnvironmentNoNetwork.
#define kPayPalEnvironment PayPalEnvironmentProduction

//@interface PayPalViewController ()  {
//    NSURLConnection *currentConnection;
//}

//@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

//@property (nonatomic, strong) NSString *items;
//@property (nonatomic, strong) NSString *details;

//@property(nonnull, nonatomic, strong) NSString *name;
//@property(nonatomic, assign, readwrite) NSUInteger quantity;
//@property(nonnull, nonatomic, copy, readwrite) NSDecimalNumber *price;
//@property(nonnull, nonatomic, copy, readwrite) NSString *currency;
//@property(nullable, nonatomic, copy, readwrite) NSString *sku;

//@property(nonnull, nonatomic, strong) NSString *invoice;
//@property(nonnull, nonatomic, copy, readwrite) NSDecimalNumber *shipping;
//@property(nonnull, nonatomic, copy, readwrite) NSDecimalNumber *tax;
//@property(nonnull, nonatomic, copy, readwrite) NSString *detailsCurrency;
//@property(nonnull, nonatomic, strong) NSString *order_description;

//@property (nonatomic, strong) NSMutableArray *itemsArray;
//@property(nonatomic, assign, readwrite) NSUInteger payPalCallBackId;

//@end

@implementation PayPalViewController

-(instancetype) initWithItems:(NSString*) items andDetails:(NSString *) details
{    
    NSError *jsonError;
    NSData *itemsData   = [items dataUsingEncoding:NSUTF8StringEncoding];
//    _itemsArray = [NSJSONSerialization JSONObjectWithData:itemsData
//                                                  options:NSJSONReadingMutableContainers
//                                                    error:&jsonError];
    
    NSData *detailsData        = [details dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *detailsArray = [NSJSONSerialization JSONObjectWithData:detailsData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
    
    //Details
    
//    _invoice           = detailsArray[@"invoice"];
//    _shipping          = [NSDecimalNumber decimalNumberWithString:detailsArray[@"shipping"]];
//    _tax               = [NSDecimalNumber decimalNumberWithString:detailsArray[@"tax"]];
//    _detailsCurrency   = detailsArray[@"currency"];
//    _order_description = detailsArray[@"order_description"];
    
    return self;
}
- (void)viewDidLoad {
//    [super viewDidLoad];
    NSDictionary *infoPlistDict = [[NSBundle mainBundle] infoDictionary];
    NSString *payPalClientId = infoPlistDict[@"PayPalClientID"];
    
    NSLog(@"payPalClientId %@", payPalClientId);
    
//    [PayPalMobile initializeWithClientIdsForEnvironments:@{kPayPalEnvironment : payPalClientId}];
//    // Set up payPalConfig
//    _payPalConfig = [[PayPalConfiguration alloc] init];
//    _payPalConfig.acceptCreditCards = YES;
//
//    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
//    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
//    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
//
//    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
//
//    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // use default environment, should be Production in real life
//    self.environment = kPayPalEnvironment;
}

- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:YES];
    // Preconnect to PayPal early
//    [self setPayPalEnvironment:self.environment];
}

- (void)setPayPalEnvironment:(NSString *)environment {
//    self.environment = environment;
//    [PayPalMobile preconnectWithEnvironment:environment];
}

#pragma mark - Receive Single Payment

//- (PayPalPayment *) payment {
//
//    // Create a PayPalPayment
//
//    NSMutableArray *items = [[NSMutableArray alloc] init];
//    for (NSDictionary *itemsDict in _itemsArray) {
//
//        _name     = itemsDict[@"name"];
//        _quantity = [itemsDict[@"quantity"] integerValue];
//        _price    = [NSDecimalNumber decimalNumberWithString:itemsDict[@"price"]];
//        _currency = itemsDict[@"currency"];
//        _sku      = itemsDict[@"sku"];
//
//        PayPalItem *item = [PayPalItem itemWithName:_name
//                                       withQuantity:_quantity
//                                          withPrice:_price
//                                       withCurrency:_currency
//                                            withSku:_sku];
//
//        [items addObject:item];
//    }
//
//    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
//
//    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
//                                                                               withShipping:_shipping
//                                                                                    withTax:_tax];
//
//    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:_shipping] decimalNumberByAdding:_tax];
//
//    // Amount, currency, and description
//    PayPalPayment *payment   = [[PayPalPayment alloc] init];
//    payment.amount           = total;
//    payment.currencyCode     = _detailsCurrency;
//    payment.shortDescription = _order_description;
//    payment.items            = items;  // if not including multiple items, then leave payment.items as nil
//    payment.paymentDetails   = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
//
//    payment.intent = PayPalPaymentIntentSale;
//
//    payment.invoiceNumber = _invoice;
//
//    // Check whether payment is processable.
//    if (!payment.processable) {
//    }
//
//    return payment;
//}

@end
