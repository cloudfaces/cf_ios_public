//
//  CFTabItem.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "BSJsonSupported.h"

@interface CFTabItem : NSObject <BSJsonSupported>

@property (strong, nonatomic, readonly) CFPage* cfPage;
@property (copy, nonatomic) NSString* title;
@property (strong, nonatomic) UIImage* icon;
@property (copy, nonatomic) NSString* iconName;

- (id) initWithPage:(CFPage*) page;

@end
