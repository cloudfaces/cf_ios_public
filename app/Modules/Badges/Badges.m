//
//  Badges.m
//  app
//
//  Created by LeyLa Mehmed on 1/24/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "Badges.h"
#import "AppDelegate.h"
@implementation Badges

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
bool allowNotif;
bool allowsSound;
bool allowsBadge;
bool allowsAlert;


+ (Badges *)sharedInstance {
    static dispatch_once_t onceToken;
    static Badges *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[Badges alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void) getBadge {
    
    // Schedule the notification
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    [self setNotificationTypesAllowed];
    
    localNotification.alertBody = @"Badge number +1";
    localNotification.alertAction = @"Show me the item";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
}

- (NSString *) updateBadgeNumber:(NSString *) badgeNumber{
    
    // Schedule the notification
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    [self setNotificationTypesAllowed];
    
    localNotification.alertBody = [NSString stringWithFormat: @"%d", [badgeNumber intValue] ] ;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [badgeNumber intValue];
    _badgeNum = [badgeNumber intValue];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    return badgeNumber;
}

- (NSString *) setData:(NSString *) data{
    data = [[NSString alloc] initWithString:data];
    return data;
}

- (NSString *) getBadgeNumber {
    
    NSString *strFromInt = [NSString stringWithFormat:@"%ld",_badgeNum];
    
    return strFromInt;
    
}

- (void)setNotificationTypesAllowed
{
    // get the current notification settings
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    allowNotif = (currentSettings.types != UIUserNotificationTypeNone);
    allowsSound = (currentSettings.types & UIUserNotificationTypeSound) != 0;
    allowsBadge = (currentSettings.types & UIUserNotificationTypeBadge) != 0;
    allowsAlert = (currentSettings.types & UIUserNotificationTypeAlert) != 0;
}

@end

