//
//  CFHtmlPage.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "TypedPage.h"
#import "BSJsonSupported.h"

@interface CFHtmlPage : NSObject <TypedPage, BSJsonSupported>

@property (readonly, nonatomic) NSString* source;
@property (readonly, nonatomic) UIColor* backgroundColor;
@property (readonly, nonatomic) BOOL fullScreen;

- (id) initWithPage:(CFPage*) page;

@end
