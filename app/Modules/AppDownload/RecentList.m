//
//  RecentList.m
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "RecentList.h"

#define RENCENT_LIST_SIZE 10

@interface RecentList ()

@property (nonatomic, strong) NSString * dynamicPlistPath;

@end

@implementation RecentList


- (id)init {
	if (self = [super init]) {
		// Look in Documents for an existing plist file
		NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		self.dynamicPlistPath = [documentsDirectory stringByAppendingPathComponent:@"data.plist"];
        
		// If it's not there, copy it from the bundle
		NSFileManager *fileManager = [NSFileManager defaultManager];
		if (![fileManager fileExistsAtPath:self.dynamicPlistPath] ) {
			NSString *pathToDefaultPlist = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
			NSError *error;
			if ([fileManager copyItemAtPath:pathToDefaultPlist toPath:self.dynamicPlistPath error:&error] == NO) {
				//NSAssert1(0, @"Failed to copy data with error message '%@'.", [error localizedDescription]);
			}
		}
	}
	return self;
}

+ (RecentList*) getInstance {
	static RecentList *instance;
	
	if (!instance) {
		instance = [[RecentList alloc] init];
	}
	
	return instance;
}

- (NSArray*) getRecentList
{
    NSMutableDictionary *mPList = [NSMutableDictionary dictionaryWithContentsOfFile:self.dynamicPlistPath];
	NSArray* result = [mPList valueForKey:@"recentlist"];
    if (!result) {
        result = [[NSArray alloc] init];
    }
    
    return result;
}

- (void) setRecentList:(NSArray*) recentList
{
    NSMutableDictionary *mPList = [NSMutableDictionary dictionaryWithContentsOfFile:self.dynamicPlistPath];
	[mPList setValue:recentList forKey:@"recentlist"];
	[mPList writeToFile:self.dynamicPlistPath atomically:YES];
}


- (void) addEntry:(NSString*) entryValue {
    
    NSMutableArray* recentList = [[self getRecentList] mutableCopy];
    
    if ([recentList containsObject:entryValue]) {
        [recentList removeObject:entryValue];
    }
    [recentList insertObject:entryValue atIndex:0];
    
    while (recentList.count > RENCENT_LIST_SIZE) {
        [recentList removeObjectAtIndex:RENCENT_LIST_SIZE];
    }
    
    [self setRecentList:recentList];
}

@end
