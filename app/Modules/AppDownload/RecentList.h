//
//  RecentList.h
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecentList : NSObject

+ (RecentList*) getInstance;

- (NSArray*) getRecentList;
- (void) setRecentList:(NSArray*) recentList;

- (void) addEntry:(NSString*) entryValue;

@end
