//
//  NavigationWebViewStack.m
//  app
//
//  Created by LeyLa on 4/24/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "NavigationWebViewStack.h"

@implementation NavigationWebViewStack

+ (instancetype) instance {
    static NavigationWebViewStack *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _webViews = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) setWebView:(UIViewController*)webView andPageId:(NSString*)pageId {
    NSDictionary * webViewDict = [[NSDictionary alloc] initWithObjectsAndKeys:webView, @"webView", pageId, @"pageId", nil];
    [_webViews addObject:webViewDict];
}

@end
