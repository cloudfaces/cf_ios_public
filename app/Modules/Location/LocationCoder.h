//
//  LocationCoder.h
//  app
//
//  Created by Bernhard Kunnert on 11.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationCoder : NSObject

+ (NSDictionary*) dictionaryFromLocation:(CLLocation*) location;
+ (NSString*) stringFromLocation:(CLLocation*) location;

+ (NSDictionary*) dictionaryFromCircularRegion:(CLCircularRegion*) region;

@end
