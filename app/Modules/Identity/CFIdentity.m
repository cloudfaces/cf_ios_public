//
//  CFIdentity.m
//  app
//
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFIdentity.h"

#import <AdSupport/ASIdentifierManager.h>


@implementation CFIdentity

+ (NSString*)advertisingIdentifier
{
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

@end
