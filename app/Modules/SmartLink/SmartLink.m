//
//  SmartLink.m
//  app
//
//  Created by LeyLa on 5/16/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "SmartLink.h"
//#import <SmartlinkLib/SmartlinkLib.h>
#import "HFSmartLink.h"
#import "HFSmartLinkDeviceInfo.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <NetworkExtension/NetworkExtension.h>

@implementation SmartLink {
    HFSmartLink * smtlk;
    BOOL isconnecting;
}

@synthesize delegate;

+ (SmartLink *)sharedInstance {
    static dispatch_once_t onceToken;
    static SmartLink *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[SmartLink alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(void) connectSsid: (NSString *) ssid andPassword:(NSString *) password andTiomeout:(NSNumber *) timeout {
    
    // Do any additional setup after loading the view, typically from a nib.
    smtlk = [HFSmartLink shareInstence];
    smtlk.isConfigOneDevice = true;
    isconnecting = false;
    self.ssid = ssid;
    [self showWifiSsid];
    
    long int waitTimers = [timeout integerValue] / 1000;
    smtlk.waitTimers = waitTimers;
    _password = [[NSString alloc] initWithString:password];
}

- (void)didReceiveMemoryWarning {
    // [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [smtlk closeWithBlock:^(NSString *closeMsg, BOOL isOK) {
        if(!isOK)
            NSLog(@"%@",closeMsg);
    }];
}
- (void)connectPress {
    
    NSString * pswdStr = _password;
    if(!isconnecting){
        
        
        [smtlk startWithKey:pswdStr processblock:^(NSInteger process) {
            
            
        } successBlock:^(HFSmartLinkDeviceInfo *dev) {
            
            
            NSDictionary *result = [[NSDictionary alloc] initWithObjectsAndKeys:dev.mac,@"mac", dev.ip, @"ip", nil];
            
            //Arguments
            if (self.delegate) {
                
                [self.delegate onLinked:result];
                
                NSString *completed = @"completed";
                [self.delegate onCompleted:completed];
                
            }
            
        } failBlock:^(NSString *failmsg) {
            // [self  showAlertWithMsg:failmsg title:@"errora"];
            if (self.delegate) {
                
                [self.delegate onTimeOut:@"timeout"];
                [self didReceiveMemoryWarning];
                
            }
            
        } endBlock:^(NSDictionary *deviceDic) {
            isconnecting  = false;
            
        }];
        isconnecting = true;
        //   [self.connectBtn setTitle:@"connecting" forState:UIControlStateNormal];
    }else{
        [smtlk stopWithBlock:^(NSString *stopMsg, BOOL isOk) {
            if(isOk){
                isconnecting  = false;
                [self showAlertWithMsg:stopMsg title:@"OK"];
                [self didReceiveMemoryWarning];
                
                //[self.connectBtn setTitle:@"connect" forState:UIControlStateNormal];
                //[self showAlertWithMsg:stopMsg title:@"OK"];
            }else{
                [self showAlertWithMsg:stopMsg title:@"errоr"];
                [self didReceiveMemoryWarning];
                
                //[self showAlertWithMsg:stopMsg title:@"Please, unplug and plug your Melissa and try again."];
                
            }
        }];
    }
    
}


-(void)showAlertWithMsg:(NSString *)msg
                  title:(NSString*)title{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
    [alert show];
}

- (void)showWifiSsid
{
    BOOL wifiOK= FALSE;
    NSDictionary *ifs;
    NSString *ssid;
    UIAlertView *alert;
    if (!wifiOK)
    {
        ifs = [self fetchSSIDInfo];
        ssid = self.ssid;
        
        if (ssid!= nil)
        {
            wifiOK = TRUE;
            self.ssid = ssid;
            
        }
        else
        {
            alert= [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Please connect Wi-Fi"] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            alert.delegate=self;
            [alert show];
        }
    }
}

- (id)fetchSSIDInfo {
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    NSLog(@"Supported interfaces: %@", ifs);
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%@ => %@", ifnam, info);
        if (info && [info count]) { break; }
    }
    return info;
}

-(void)savePswd{
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    [def setObject:self.pswd forKey:self.ssid];
}
//-(NSString *)getspwdByssid:(NSString * )mssid{
//    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
//    return [def objectForKey:mssid];
//}




//Captive

- (BOOL)connectedToInternet
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:@"http://www.google.com/"]];
    
    [request setHTTPMethod:@"HEAD"];
    
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}

- (void)yourMethod
{
    if([self connectedToInternet] == NO)
    {
        // Not connected to the internet
        
        NSLog(@"NO");
    }
    else
    {
        // Connected to the internet
        NSLog(@"Yes");

    }
}


//-(NSString *)getspwdByssid:(NSString * )mssid{
//    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
//    return [def objectForKey:mssid];
//}



@end
