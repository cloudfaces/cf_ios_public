//
//  SmartLink.h
//  app
//
//  Created by LeyLa on 5/16/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFSmartLink.h"
#import <WebKit/WebKit.h>

@protocol SmartLinkDelegate <NSObject>

- (void) onTimeOut:(NSString *) timeOut;
- (void) onCompleted:(NSString *) onCompleted;
- (void) onLinked:(NSDictionary *) args;

@end

@interface SmartLink : NSObject <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>


@property (weak, nonatomic)  id<SmartLinkDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;

+ (SmartLink *)sharedInstance;
- (instancetype) init;
- (void)connectPress;

- (void) connectSsid: (NSString *) ssid andPassword:(NSString *) password andTiomeout:(NSNumber *) timeout;
- (id) fetchSSIDInfo;
@property (nonatomic, strong) NSString *ssid;
@property (nonatomic, strong) NSString *pswd;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSNumber *timeOut;


@end
