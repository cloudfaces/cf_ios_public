//
//  CFContentView.h
//  app
//
//  Created by LeyLa on 3/8/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSJsonSupported.h"

@interface CFContentView : NSObject <BSJsonSupported>

@property(nonatomic, copy) NSString *type;
@property(nonatomic, strong) UIColor *color;
@property(nonatomic, copy) NSString *text;

@end
