//
//  CFPageHeader.h
//  app
//
//  Created by Bernhard Kunnert on 06.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFHeaderButton.h"
#import "CFContentView.h"
#import "BSJsonSupported.h"

@interface CFPageHeader : NSObject <BSJsonSupported>

@property(nonatomic, strong) UIColor* backgroundColor;
@property(nonatomic, strong) UIColor* titleColor;
@property(nonatomic, strong) UIImage* backgroundImage;
@property(nonatomic, strong) UIImage* titleImage;
@property(nonatomic, copy) NSString* backgroundImageName;
@property(nonatomic, strong) NSString* titleImageName;
@property(nonatomic, strong) UIColor* statusBarColor;

@property(nonatomic, strong) CFHeaderButton* rightButton;
@property(nonatomic, strong) CFHeaderButton* leftButton;
@property(nonatomic, strong) CFContentView* contentView;

@property(nonatomic, strong) NSArray* rightButtonsArray;

@end
