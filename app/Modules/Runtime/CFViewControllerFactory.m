//
//  CFViewControllerFactory.m
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFViewControllerFactory.h"
#import "WebviewViewController.h"
#import "TabBarViewController.h"
#import "SlidingViewController.h"
#import "SlidingTabViewController.h"

@implementation CFViewControllerFactory

- (UIViewController<Navigation>*) viewControllerForPage:(CFPage *)page {
    UIViewController<Navigation>* viewController;
    if ([page.type isEqualToString:@"html"]) {
        viewController = [[WebviewViewController alloc] init];
    } else if ([page.type isEqualToString:@"tab"]) {
        viewController = [[TabBarViewController alloc] init];
    } else if ([page.type isEqualToString:@"navdrawer"]) {
        viewController = [[SlidingViewController alloc] init];
    } else if ([page.type isEqualToString:@"slidingtab"]) {
        viewController = [[SlidingTabViewController alloc] init];
    }
    return viewController;
}

@end
