//
//  CFDynamicFileUpdate.m
//  app
//
//  Created by LeyLa on 7/3/17.
//  Copyright © 2017 CloudFaces. All rights reserved.
//

#import "CFDynamicFileUpdate.h"
#include <CommonCrypto/CommonDigest.h>
#import "AppDownloadViewController.h"
#import "MRProgress.h"
#import "SplashViewController.h"
#import <SSZipArchive.h>

BOOL result;
NSString *bundleIdentifier;
NSString *wwwTempDir;
NSString *wwwDir;
NSString *wwwDirOld;
UIViewController *topView;
NSString *manifestPath;
NSString *manifestOld;
NSString *objectUrl;
NSString *appHashUrl;
NSString *nativeVersion;
NSString *manifestNewFile;
NSDictionary *jsonDict;
int filesCount = 0;
int filesCountArray = 0;
int a = 0;
int b = 0;

@implementation CFDynamicFileUpdate
+ (CFDynamicFileUpdate *)sharedInstance {
    static dispatch_once_t onceToken;
    static CFDynamicFileUpdate *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CFDynamicFileUpdate alloc] init];
        topView = [UIApplication sharedApplication].keyWindow.rootViewController;
        [MRProgressOverlayView showOverlayAddedTo:topView.view animated:YES];
        //Objects Url
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void) downoadManifest: (NSString *) stringSha1 {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        
        //objectUrl = @"https://cdn.cloudfaces.com/objects/";
        NSString *osbu = [[NSUserDefaults standardUserDefaults] stringForKey:@"object_storage_base_url"];
        objectUrl = [osbu stringByAppendingString: @"/objects/"];
        
        NSLog(@"CodePush - CFDynamicFileUpload - downalodManifest - objectUrl %@", objectUrl);
        
        //Check if localCach is existing
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *localCacheFolderExist = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"/local_cache"]];

        if ([[NSFileManager defaultManager] fileExistsAtPath:localCacheFolderExist]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"codePushFileCompare"];
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstCodePush"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"firstCodePush"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }

    //Copy www and content folder to app_bundle_id/
   // wwwDir = [documentsDirectory stringByAppendingPathComponent: @"/www"];
  //  wwwTempDir = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"/www_temp"]];

  //  NSError *error;
  //  BOOL success = [[NSFileManager defaultManager] copyItemAtPath:wwwDir toPath:wwwTempDir error:&error];

  //  if (success){
  //      NSLog(@"success success");
  //  }

        NSString *folderNameManifest = [stringSha1 substringToIndex:2];
        NSString *fileNameManifest = [stringSha1 substringWithRange:NSMakeRange(2, [stringSha1 length]-2)];

        NSString *objectDirectory = [NSString stringWithFormat: @"%@/%@", folderNameManifest, fileNameManifest];
        NSString *manifestObjectUrlFull = [NSString stringWithFormat: @"%@%@", objectUrl, objectDirectory];

        NSString *path = manifestObjectUrlFull;
        NSURL *url = [NSURL URLWithString:path];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        NSDictionary *s = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];

        NSLog(@"json Manifest New ssss: %@", s);
        jsonDict = s;
        NSLog(@"jsonDict Manifest New: %@", jsonDict);
        NSLog(@"Manifest Downloadeddd ");

        [self readManifestFromJson:jsonDict];
        NSLog(@"Manifest READDED ");
    });
}

- (void) downoadFileToLocalCacheFromSha1: (NSString *) stringSha1 andCopyTo: (NSString *) pathDir {
    bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];

    NSString *folderName = [stringSha1 substringToIndex:2];
    NSString *fileName = [stringSha1 substringWithRange:NSMakeRange(2, [stringSha1 length]-2)];

    //NSString *objectUrl = @"https://cdn.cloudfaces.com/objects/";
    NSString *osbu = [[NSUserDefaults standardUserDefaults] stringForKey:@"object_storage_base_url"];
    objectUrl = [osbu stringByAppendingString: @"/objects/"];
        
    NSString *objectDirectory = [NSString stringWithFormat: @"%@/%@", folderName, fileName];
    NSString *objectUrlFull = [NSString stringWithFormat: @"%@%@", objectUrl, objectDirectory];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSString *filePath = [NSString stringWithFormat:@"%@/%@/%@", _localCachePath, folderName, fileName];

        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];

        if (fileExists) {
            [self copyFileToTempFromPath:filePath toPathDir:pathDir];
        }else{
                //saving is done on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    result = [[CFRuntime instance] downloadAndReplaceLocalFile:filePath withContentOfUrl:[NSURL URLWithString:objectUrlFull]];
                    if (result) {
                        NSLog(@"File Saved!");
                        //if validateFile = true
                        NSString *codePushFileCompare = [[NSUserDefaults standardUserDefaults] stringForKey:@"codePushFileCompare"];
                        if ([codePushFileCompare isEqual:@"YES"]) {
                            //Check SHA
                            //BOOL isEqual =  [self compareDownloadedSha1:filePath withSha1:stringSha1];

                            if ([self compareDownloadedSha1:filePath withSha1:stringSha1]) {

                                NSLog(@"File DOWNLOADED AND  Compare sha1 ---- copy from  %@  to %@ ", filePath, pathDir);

                                //Copy to www_temp
                                //  [self copyFileToTemp:pathDir];
                                [self copyFileToTempFromPath:filePath toPathDir:pathDir];
                            }
                        }  else {
                            //else just copyFileToTempFromPath without compareDownloadedSha1
                            [self copyFileToTempFromPath:filePath toPathDir:pathDir];
                        }
                    } else {
                        _errorMsg = @"Error downloading file!";
                        NSLog(@"Message: %@ ",_errorMsg );
                        _isOK = @"false";
                    }
                });
        }
    });
}

- (void) readManifestFromJson:(NSDictionary *) jsonDict {
    //Check if localCach is existing
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *localCacheFolderExist = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"/local_cache"]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:localCacheFolderExist]) {
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //update your UI
        NSString *zipPath = @"";

        appHashUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"app_hash_url"];

        //Get app hash from url
        NSString *code = [appHashUrl substringFromIndex: [appHashUrl length] - 65];
        NSString *appHash = [code substringToIndex:64];
        NSLog(@"CodePush - sliced appHash from url is  %@", appHash);
        
        //NSString *stringURL = [NSString stringWithFormat:@"https://cdn.cloudfaces.com/objects_zips/%@/objects.zip", appHash];
        NSString *osbu = [[NSUserDefaults standardUserDefaults] stringForKey:@"object_storage_base_url"];
        NSString *stringURL = [osbu stringByAppendingString: @"/objects_zips/"];
        stringURL = [stringURL stringByAppendingString: appHash];
        stringURL = [stringURL stringByAppendingString: @"/objects.zip"];
                
        NSURL  *url = [NSURL URLWithString:stringURL];

            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];

            zipPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"objects.zip"];
            [[NSData dataWithContentsOfURL:url] writeToFile:zipPath atomically:YES];

        [SSZipArchive unzipFileAtPath:zipPath toDestination:localCacheFolderExist];
    }
    _localCachePath = localCacheFolderExist;

    NSArray *files;
    NSArray *manifestNewArray;
    NSArray *manifestOldArray;

    manifestNewArray = jsonDict[@"files"];

    NSDictionary *manifestOld = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"manifestOld"] mutableCopy];
    manifestOldArray = manifestOld[@"files"];

    if ((manifestOld[@"files"]  == nil) || ([manifestOld[@"files"] isKindOfClass:[NSNull class]])|| manifestOldArray.count == 0)
    {
        files = manifestNewArray;
        NSLog(@"Files without OLD Manifest %@ ", files);
    } else {
        NSMutableSet *setOne = [NSMutableSet setWithArray: manifestNewArray];
        NSMutableSet *setTwo = [NSMutableSet setWithArray: manifestOldArray];

        [setOne minusSet: setTwo];
        NSArray *arrayOneResult = [setOne allObjects];
        
        files = arrayOneResult;
    }

    if (files.count == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isManifest"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        //Stop progress
        [MRProgressOverlayView dismissOverlayForView:topView.view animated:YES];
        UIWindow* window = [UIApplication sharedApplication].keyWindow;
        if (!window)
            window = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [MRProgressOverlayView dismissOverlayForView:[[window subviews] objectAtIndex:0].superview animated:YES];
        
        //ReloadApp
        [[AppDownloadViewController alloc] initRuntimeFromUrl:nil];
    } else {
        for (NSDictionary *file in files) {
            filesCountArray = (int) files.count;
            NSString *path = [file objectForKey:@"path"];
            NSString *objectKey = [file objectForKey:@"object_key"];
            [self downoadFileToLocalCacheFromSha1:objectKey andCopyTo:path];
        }
    }
}

- (BOOL)renameFileWithName:(NSString *)srcName toName:(NSString *)dstName
{
    NSError *error = nil;
    NSFileManager *filemgr = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:srcName];
    NSString *filePath2 = [documentsDirectory stringByAppendingPathComponent:dstName];
    
    // Attempt the move
    if ([filemgr moveItemAtPath:filePath toPath:filePath2 error:&error] != YES){
        NSLog(@"Unable to move file: %@", [error localizedDescription]);
        return false;
    } else {
        return true;
    }
}


- (BOOL) compareDownloadedSha1: (NSString *) downloadedFileStr withSha1: (NSString *)sha1 {
    BOOL result = false;
    NSString *fileSha1 = [self shaHashOfPath:downloadedFileStr];
    if ([fileSha1 isEqual:@""]) {
        _errorMsg = @"File SHA1 is empty!";
        NSLog(@"Message: %@ ",_errorMsg );
        result = false;
    } else {
        if ([fileSha1 isEqual:sha1]) {
            result = true;
        } else {
            _errorMsg = @"File SHA1 is not equal to sha1 string!";
            NSLog(@"Message: %@ ",_errorMsg );
            result = false;
        }
    }
    return result;
}

- (NSString *)shaHashOfPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // Make sure the file exists
    if( [fileManager fileExistsAtPath:path isDirectory:nil] )
    {
        NSData *data = [NSData dataWithContentsOfFile:path];
        unsigned char digest[CC_SHA1_DIGEST_LENGTH];
        CC_SHA1( data.bytes, (CC_LONG)data.length, digest );
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
        for( int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++ )
        {
            [output appendFormat:@"%02x", digest[i]];
        }
        return output;
    } else {
        return @"";
    }
}

-(void)createDirectory:(NSString *)directoryName atFilePath:(NSString *)filePath
{
    NSString *filePathAndDirectory = [filePath stringByAppendingPathComponent:directoryName];
    NSError *error;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                   withIntermediateDirectories:YES
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }
}

- (void) copyFileToTempFromPath:(NSString *)fromDir toPathDir:(NSString *) pathDir
{
    _isOK = @"true";
    BOOL success = false;
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath  =  [NSString stringWithFormat: @"%@/www%@", documentsDirectory, pathDir];
    NSLog(@"LM pathDir %@ ", pathDir);
    NSLog(@"LM filePath %@ ", filePath);
    NSString *pathStr =  [filePath stringByDeletingLastPathComponent];
    if (![[NSFileManager defaultManager] createDirectoryAtPath:pathStr
                                   withIntermediateDirectories:YES
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }

    if ([[NSFileManager defaultManager] fileExistsAtPath: filePath])
        [[NSFileManager defaultManager] removeItemAtPath: filePath error:nil];

    success = [fileManager copyItemAtPath:fromDir toPath:filePath error:&error];

    if (success) {
        a++;
        NSLog(@"Success! copyItemAtPath ");

        filesCount = a+b;

        if (filesCountArray == filesCount) {
            [[NSUserDefaults standardUserDefaults] setObject:jsonDict forKey:@"manifestOld"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSUserDefaults standardUserDefaults] setObject:_appVersionApiCall forKey:@"app_version"];

            _isManifest = @"NO";
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isManifest"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            //Stop progress
            [MRProgressOverlayView dismissOverlayForView:topView.view animated:YES];
            UIWindow* window = [UIApplication sharedApplication].keyWindow;
            if (!window)
                window = [[UIApplication sharedApplication].windows objectAtIndex:0];
            [MRProgressOverlayView dismissOverlayForView:[[window subviews] objectAtIndex:0].superview animated:YES];

            //ReloadApp
            [[AppDownloadViewController alloc] initRuntimeFromUrl:nil];
        }
    } else {
        b++;
        NSLog(@"ERROR copyItemAtPath %@",error);
    }
}

@end
