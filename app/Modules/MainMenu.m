//
//  MainMenu.m
//
//  Created by Simon Moser on 22.08.11.
//  Copyright 2011 bluesource - mobile solutions gmbh. All rights reserved.
//
#import "MainMenu.h"
#import "SplashViewController.h"

@implementation MainMenu

- (id)init {

    self = [super initWithRootViewController:[[SplashViewController alloc] init]];
	if (self) {
	}
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.navigationBarHidden = YES;
}

//- (void) viewDidAppear:(BOOL)animated
//{
//    NSLog(@"[UIApplication sharedApplication].keyWindow1 %@", [UIApplication sharedApplication].keyWindow);
//    [super viewDidLoad];
//    self.navigationBarHidden = YES;
//    [super viewDidAppear:animated];
//}

@end
