//
//  CFSlidingTabItem.h
//  app
//
//  Created by LeyLa on 4/30/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "BSJsonSupported.h"

@interface CFSlidingTabItem : NSObject <BSJsonSupported>

@property (strong, nonatomic, readonly) CFPage* cfPage;
@property (copy, nonatomic) NSString* title;
@property (strong, nonatomic) UIImage* icon;
@property (copy, nonatomic) NSString* iconName;

- (id) initWithPage:(CFPage*) page;

@end
