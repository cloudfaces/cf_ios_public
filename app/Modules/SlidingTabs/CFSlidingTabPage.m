//
//  CFSlidingTabPage.m
//  app
//
//  Created by LeyLa on 4/30/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import "CFSlidingTabPage.h"
#import "CFRuntime.h"
#import "CFSlidingTabItem.h"
#import "UIImage+Asset.h"
#import "NSString+NSString_Additions.h"
#import "CFPage+JSON.h"

@interface CFSlidingTabPage ()

@property (strong, nonatomic, readwrite) CFPage* page;

@end

@implementation CFSlidingTabPage

#pragma mark BSJsonSupported

- (id) proxyForJson
{
    NSMutableDictionary* pageDict = [[self.page proxyForJson] mutableCopy];

    NSMutableArray* tabArray = [NSMutableArray arrayWithCapacity:self.tabItems.count];
    for (CFSlidingTabItem* tabItem in self.tabItems) {
        [tabArray addObject:[tabItem proxyForJson]];
    }

    [pageDict setObject:tabArray forKey:@"tabItems"];

    return pageDict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }

        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];

        CFPage* innerPage = [[CFPage alloc] initWithJson:receivedObjects];

        NSArray* tabs = receivedObjects[@"tabItems"];
        NSMutableArray* tabItems = [NSMutableArray arrayWithCapacity:tabs.count];

        for (NSDictionary* itemDict in tabs) {
            CFSlidingTabItem* tabItem = [[CFSlidingTabItem alloc] initWithJson:itemDict];
            [tabItems addObject:tabItem];
        }

        self.page = innerPage;
        self.tabItems = tabItems;
    }

    return self;
}


- (id) initWithPage:(CFPage *)page {
    self = [super init];
    if (self) {
        self.page = page;

        self.tabItems = [self readTabItems];
        self.navbarBackgroudColor = [self readNavbarBackgroudColor];
        self.iconColor = [self readIconColor];
        self.iconColorSelected = [self readIconSelectedColor];

    }

    return self;
}

- (NSString *) readNavbarBackgroudColor {
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];

    NSError* error;
    NSArray* elements = [pageElement nodesForXPath:@"./tabview/@navbarBackgroudColor" error:&error];

    NSString *navbarBackgroudColor;

    if (elements && elements.count > 0) {
        navbarBackgroudColor = [elements[0] stringValue];
    }
    return navbarBackgroudColor;
}

- (NSString *) readIconColor {
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];

    NSError* error;
    NSArray* elements = [pageElement nodesForXPath:@"./tabview/@iconColor" error:&error];

    NSString *iconColor;

    if (elements && elements.count > 0) {
        iconColor = [elements[0] stringValue];
    }

    return iconColor;
}

- (NSString *) readIconSelectedColor {
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
//
   NSError* error;
    NSArray* elements = [pageElement nodesForXPath:@"./tabview/@iconColorSelected" error:&error];

    NSString *iconColorSelected;


    if (elements && elements.count > 0) {
        iconColorSelected = [elements[0] stringValue];
    }
    return iconColorSelected;
}

- (NSArray*) readTabItems {
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];

    NSError* error;
    NSArray* tabElements = [pageElement nodesForXPath:@"./tabview/tabitem" error:&error];
    NSMutableArray* tabItems = [NSMutableArray arrayWithCapacity:tabElements.count];

    for (id tabElement in tabElements) {
        CFPage* tabPage = [[CFRuntime instance] getPageWithId:[[tabElement attributeForName:@"content"] stringValue]];
        NSString* title = [[tabElement attributeForName:@"title"] stringValue];

        if ([NSString isEmpty:title]) {
            title = tabPage.title;
        }

        NSString* iconString = [[tabElement attributeForName:@"icon"] stringValue];

        CFSlidingTabItem* tabItem = [[CFSlidingTabItem alloc] initWithPage:tabPage];
        tabItem.title =title;
        if (![NSString isEmpty:iconString]) {
            tabItem.iconName = iconString;
            tabItem.icon = [UIImage imageFromAsset:iconString];
        }

        [tabItems addObject:tabItem];
    }

    return tabItems;
}

@end
