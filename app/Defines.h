//
//  Defines.h
//  mobile-pocket
//
//  Created by Simon Moser on 04.11.11.
//

#ifndef app_Defines_h
#define app_Defines_h


// fixed values
#define DATABSE_NAME @"Database.sqlite"
#define PUSH_NOTIFCATION_CHANGED @"PUSH_NOTIFCATION_CHANGED"
#define PUSH_NOTIFCATION_CHANGED @"PUSH_NOTIFCATION_CHANGED"
#define HTTP_HEADER_ACCEPT_LANGUAGE @"Accept-Language"

#define INTERFACE_IS_PAD     ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) 
#define INTERFACE_IS_PHONE   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 

#define BSStaticTargetConfig(keyName) ([BundleSettings getTargetConfigValueWithName:keyName])


#ifdef DEBUG
#define TRACK_METHOD_CALL()                 ((void)0)
#define TRACK_EVENT(eventName)              ((void)0)
#define TRACK_PAGE_VIEW(controller)        ((void)0)
#else
#define TRACK_METHOD_CALL()                 [[[GAI sharedInstance] defaultTracker] sendEventWithCategory:@"uiAction" withAction:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] withLabel:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] withValue:nil];
#define TRACK_EVENT(eventName)              [[[GAI sharedInstance] defaultTracker] sendEventWithCategory:@"uiAction" withAction:eventName withLabel:eventName withValue:nil];
#define TRACK_PAGE_VIEW() id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker]; [tracker set:kGAIScreenName value:[self screenTrackingName]];[tracker send:[[GAIDictionaryBuilder createAppView] build]];
#endif // #ifdef FEATURE_FLURRY_TRACKING


#define UIColorFromHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#endif
