//
//  RIButtonItem+Action.h
//  Pods
//
//  Created by Bernhard Kunnert on 04.11.13.
//
//

#import "RIButtonItem.h"

@interface RIButtonItem (Action)

+(id)itemWithLabel:(NSString *)inLabel andAction:(void (^)()) action;

@end
