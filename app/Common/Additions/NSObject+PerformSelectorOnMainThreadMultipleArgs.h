//
//  NSObject+PerformSelectorOnMainThreadMultipleArgs.h
//  app
//
//  Created by Bernhard Kunnert on 24.07.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (PerformSelectorOnMainThreadMultipleArgs)

-(void)performSelectorOnMainThread:(SEL)selector waitUntilDone:(BOOL)wait withObjects:(NSObject *)object, ... NS_REQUIRES_NIL_TERMINATION;

@end
