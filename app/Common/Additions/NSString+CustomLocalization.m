//
//  NSString+NSString_Additions.m
//  mobile-pocket
//
//  Created by Simon Moser on 04.11.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "NSString+CustomLocalization.h"

@implementation NSString (CustomLocalization)

+ (NSString*) localize:(NSString*) key
{
    //load the strings from a target depended table. it must always be named after the targets name
    // i.e. Localizable-mypocket.strings
    NSString* result = [[NSBundle mainBundle] localizedStringForKey:key value:@"" table:[NSString stringWithFormat:@"Localizable-%@", TARGET_ID]];
   
    if([key isEqualToString:result]) {
        //the string was no defined in the target strings so use the default strings
        result = [[NSBundle mainBundle] localizedStringForKey:key value:@"" table:nil];
    }
    return result;
}

@end
