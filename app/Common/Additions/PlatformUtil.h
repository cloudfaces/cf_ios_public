//
//  PlatformUtil.h
//  Synthesa
//
//  Created by Bernhard Kunnert on 14.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlatformUtil : NSObject

+ (BOOL) isIos7;

@end
