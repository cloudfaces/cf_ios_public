//
//  UIViewController+Additions.m
//  app
//
//  Created by Simon Moser on 13.10.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "UIViewController+Additions.h"
#import "AppDownloadviewController.h"
#import "CFRuntime.h"
#import "Navigation.h"
#import "CFDynamicFileUpdate.h"

@implementation UIViewController (Additions)

+ (UIViewController *)sharedInstance {
    static dispatch_once_t onceToken;
    static UIViewController *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[UIViewController alloc] init];

    });
    return instance;
}
- (void) initRuntimeFromUrl:(NSURL*) url
{
    if (!url) {
        url = [NSURL fileURLWithPath:[CFRuntime pathForAppFile:@"Data" ofType:@"xml" inDirectory:nil]];
    }
    
    if (![[CFRuntime instance] handleAppUpgrade])
    {
        NSLog(@"Failed to upgrade app");
    }
    
    if (![[CFRuntime instance] loadAppFromUrl:url]) {
        [BundleSettings setOverrideRemoteApp:NO];
        [[[UIAlertView alloc] initWithTitle:@"Failed" message:@"Failed to load App" cancelButtonItem:[RIButtonItem itemWithLabel:@"OK" andAction:^() {
            if ([BundleSettings isRemoteApp]) {
                [self showAppDownloadViewController];
            }
        }] otherButtonItems: nil] show];
    } else {
        NSString *isManifest = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"isManifest"];

        if ([isManifest isEqual:@"NO"]) {
            CFPage* rootPage = [[CFRuntime instance] getRootPage];

            if ([[AppDownloadViewHelper sharedInstance].overlayDebugSwitch isEqual:@"true"]) {

            [CFRuntime presentPage:rootPage withDebugoverlayInPosition:@"Left"];

            } else {
                [CFRuntime presentPage:rootPage];
            }
        }

    }
}

- (void) showAppDownloadViewController {
    AppDownloadViewController* viewController = [[AppDownloadViewController alloc] init];
    viewController.delegate = self;
    BaseNavigationController* navigation = [[BaseNavigationController alloc] initWithRootViewController:viewController];
    
    [self presentViewController:navigation animated:NO completion:nil];
}

#pragma mark AppDownloadViewControllerDelegate

- (void) viewController:(id)viewController urlSelected:(NSString *)url {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^() {
        [self initRuntimeFromUrl:[NSURL URLWithString:url]];

    }];
}

@end
