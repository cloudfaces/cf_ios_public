//
//  BaseNavigationController.m
//  app
//
//  Created by Bernhard Kunnert on 18.12.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _rotationMask = UIInterfaceOrientationMaskPortrait;
    }
    return self;
}

- (void)viewDidLoad
{
    // set the left bar button to a nice trash can
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(confirmCancel)];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}





//- (void)viewDidLoad
//{
//    // set the left bar button to a nice trash can
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(confirmCancel)];
//    [super viewDidLoad];
//}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        // The didn't press "no", so pop that view!
        [self.navigationController popViewControllerAnimated:YES];
    }
}
    - (void)confirmCancel
    {
        // Do whatever confirmation logic you want here, the example is a simple alert view
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Are you sure you want to delete your draft? This operation cannot be undone."
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
    }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) shouldAutorotate
{
    return _shouldRotate;

}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return _rotationMask;
}

-(UIStatusBarStyle)preferredStatusBarStyle{

    return [self.topViewController preferredStatusBarStyle];
}


@end
