//
//  UIViewController+Container.h
//  app
//
//  Created by Bernhard Kunnert on 07.01.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Container)

- (BOOL) isContainer;

@end
