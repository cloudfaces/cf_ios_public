////
////  BaseSlidingTabsViewController.m
////  app
////
////  Created by LeyLa on 5/3/18.
////  Copyright © 2018 CloudFaces. All rights reserved.
////
//
//#import "BaseSlidingTabsViewController.h"
//#import "BundleSettings.h"
//
//@interface BaseSlidingTabsViewController ()
//
//@end
//
//@implementation BaseSlidingTabsViewController
////@synthesize bs;
//
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//- (id)init {
//    if (self = [super init]) {
//        self.delegate = self;
//
//        NSLog(@"LMTabs BaseSliding");
//        UIImage *tabbarBgImage = [UIImage imageNamed:@"tabbar_bg.png"];
//        if(tabbarBgImage != nil) {
//            CGRect frame = CGRectMake(0, 0, 480, 49);
//            UIView *v = [[UIView alloc] initWithFrame:frame];
//            UIColor *c = [[UIColor alloc] initWithPatternImage:tabbarBgImage];
//            v.backgroundColor = c;
//            [[self tabBar] addSubview:v];
//        }
//    }
//    return self;
//}
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//- (void) makeTabBarHidden:(BOOL)hide animated:(BOOL) animated{
//    if ( [self.view.subviews count] < 2 ) {
//        return;
//    }
//
//    if(animated) {
//        [UIView beginAnimations:@"tabbarHide" context:nil];
//        [UIView setAnimationDuration:0.4];
//    }
//
//    UIView *contentView;
//
//    if ( [[self.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] ) {
//        contentView = [self.view.subviews objectAtIndex:1];
//    } else {
//        contentView = [self.view.subviews objectAtIndex:0];
//    }
//
//    if (hide) {
//        contentView.frame = self.view.bounds;
//    }
//    else {
//        contentView.frame = CGRectMake(self.view.bounds.origin.x,
//                                       self.view.bounds.origin.y,
//                                       self.view.bounds.size.width,
//                                       self.view.bounds.size.height - self.tabBar.frame.size.height);
//    }
//
//    self.tabBar.alpha = hide ? 0.0 : 1.0;
//
//    if(animated) {
//        [UIView commitAnimations];
//    }
//    self.tabBarHidden = hide;
//}
//
//@end

