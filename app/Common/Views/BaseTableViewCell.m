//
//  BaseTableViewCell.m
//  app
//
//  Created by Simon Moser on 31.07.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

// use as default the class name; prevent ugly scoll errors ;)
- (NSString *) reuseIdentifier {
    return NSStringFromClass([self class]);
}

@end
