Feature: 
Card screen related scenarios.

Background:
	Given there are images to pick with the image picker
	Given I reset the iphone app
	Given I launch the app

	
Scenario Outline: TC 4 & TC 9: Add a several retailer cards to the app.
	When I see and close the tutorial
	When I search for a cardtype "<retailerToSearch>" and touch the cardtype cell with the name "<cardTypeToSelect>"

Examples:
| retailerToSearch | cardTypeToSelect | barcodeNumber |
| eybl | eybl vorteilscard classico | 12345 |
