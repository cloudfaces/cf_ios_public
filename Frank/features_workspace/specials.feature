Feature: 
Specials related scenarios.

Background:
	Given There is an eybl card and the app is initialized
	Given I launch the app

Scenario: 
	TC 18: Open the specials tab and special details.
	When I touch the "Specials" TabBar Item
	Then I should be in the Specials screen
	Then I should see specials
	When I touch "Start Paket"
	Then I should be in the Specials Detail screen
	Then I should see "Angebot"
	Then I should see "Start Paket"
	Then I should see "270 €"



