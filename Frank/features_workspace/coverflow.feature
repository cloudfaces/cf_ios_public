Feature: 
Open card from coverflow.

Background:
	Given There is an eybl card and the app is initialized
	Given I launch the app

Scenario: 
    TC 22: Open the app rotate it an look if the coverflow is open.
   		Then I should see "eybl vorteilscard classico"
    	Then I rotate to the "left"
    	When I wait for 3 seconds
		When I wait for nothing to be animating
   		Then I should see "eybl vorteilscard classico Coverflow"
		When I touch "eybl vorteilscard classico Coverflow"
		When I wait for 3 seconds
		When I wait for nothing to be animating
		Then I should see "00012345"
	
